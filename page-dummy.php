<?php 
/* Template Name: Dummy Landing */

get_header(); ?>
<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'twodogs' ); ?></a>

<nav>
    <h1><img src='<?php echo get_theme_file_uri() ?>/images/twodogs-horizontal-transparent-cutaway-white.png' alt='Two Dogs Development'></h1>
    <ul>
      <div>
        <li><a href='/#home'>Home</a></li>
      </div>
      <div>
        <li><a href='/#services'>Services</a></li>
        <li><a href='/#portfolio'>Portfolio</a></li>
        <li><a href='/#about'>About</a></li>
      </div>
      <div>
        <li><a href='/#contact'>Contact</a></li>
        <li class='get-started'><a href='/#contact'>Get Started</a></li>
      </div>
    </ul>
  </nav>
  

    <section class='slide home' id='home'>
      <section class='heading'>
      <h2>Solutions. Synergy. Overflow. <br> Websites.</h2>
      </section>
      <section class='content'>
        <ul class='service-description-headings'>
          <li class='service'>
            <label>
              <input type='radio' value='1' name='service'/>
              <h3>Design</h3>
            </label>
          </li>
          <li class='service'>
            <label>
              <input type='radio' value='2' name='service'/>
              <h3>Development</h3>
            </label>
          </li>
          <li class='service'>
            <label>
              <input type='radio' value='3' name='service'/>
              <h3>Installation</h3>
            </label>
          </li>
          <li class='service'>
            <label>
              <input type='radio' value='4' name='service'/>
              <h3>Maintenance</h3>
            </label>
          </li>
        </ul>

        <div class='brief service-description'>
          <div class='active-service'>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
            <a href='#services' class='learn-more-link'>Learn More</a>
          </div>

          <div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, officiis fugiat. Modi, sequi dolore, ipsum blanditiis quasi ab odio saepe error consequatur natus incidunt doloremque unde optio earum molestiae fugit!</p>
            <a href='#services' class='learn-more-link'>Learn More</a>
          </div>
          
          <div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed cumque fugit maxime accusamus officiis! Tenetur, est! Magnam dolores numquam, libero excepturi quis ipsam ad atque saepe totam facere. Magnam, molestias?</p>
            <a href='#services' class='learn-more-link'>Learn More</a>
          </div>

          <div>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore sunt, aperiam fugiat ut vel iste deleniti accusantium ipsum perspiciatis sint quae, similique doloribus sequi error hic iure impedit quia ducimus.</p>
            <a href='#services' class='learn-more-link'>Learn More</a>
          </div>

        </div>

      </section>
    </section>

    <section class='slide services-panel' id='services'>
      <section class='heading'>
        <h2>What we offer</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, corrupti, alias aut quo, quibusdam cumque nostrum expedita cum nam est perferendis. Tenetur, rerum voluptate doloremque at delectus magni dolor quo.</p>
      </section>
      <section class='content light'>
        <div class='services'>
          <div>
            <div>
              <h3>Design</h3>
              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
            </div>
            <div>
              <h3>Development</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi, officiis fugiat. Modi, sequi dolore, ipsum blanditiis quasi ab odio saepe error consequatur natus incidunt doloremque unde optio earum molestiae fugit!</p>
            </div>
          </div>
          <div>
            <div>
              <h3>Installation</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed cumque fugit maxime accusamus officiis! Tenetur, est! Magnam dolores numquam, libero excepturi quis ipsam ad atque saepe totam facere. Magnam, molestias?</p>
            </div>
            <div>
              <h3>Maintenance</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore sunt, aperiam fugiat ut vel iste deleniti accusantium ipsum perspiciatis sint quae, similique doloribus sequi error hic iure impedit quia ducimus.</p>
            </div>
          </div>
        </div>
      </section>
    </section>

    <section class='slide portfolio-panel' id='portfolio'>
      <section class='heading'>
        <h2>Our Work</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, corrupti, alias aut quo, quibusdam cumque nostrum expedita cum nam est perferendis. Tenetur, rerum voluptate doloremque at delectus magni dolor quo.</p>
      </section>
      <section class='content'>
        <ul class='our-work'>
            <li>
                <button class='window'>
                    <img src='<?php echo get_theme_file_uri() ?>/images/sample_construction_window.png' alt='construction theme sample'>
                </button>
            </li>
            <li>
                <button class='window'>
                    <img src='<?php echo get_theme_file_uri() ?>/images/sample_landscape_window.png' alt='landscape theme sample'>
                </button>
            </li>
            <li>
                <button class='window'>
                    <img src='<?php echo get_theme_file_uri() ?>/images/sample_cleaning_window.png' alt='cleaning theme sample'>
                </button>
            </li>
          
        </ul>
      </section>
    </section>

    
    <section class='slide about-panel' id='about'>
      <section class='heading'>
        <h2>About Two Dogs</h2>
        <p>Two Dogs was inspired by lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil!</p>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore</p>
        
      </section>
      <section class='content'>
        <div class='about-us'>
          <div>
            <img src='<?php echo get_theme_file_uri() ?>/images/Vector.png'>
            <div>
              <h3>Jason</h3>
              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
            </div>

          </div>

          <div>
            <img src='<?php echo get_theme_file_uri() ?>/images/Vector.png'>
            <div>
              <h3>Joe</h3>
              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
            </div>
          </div>
        </div>
      </section>
    </section>

    <section class='slide contact-panel' id='contact'>
      <section class='heading'>
        <h2>Get in touch</h2>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
      </section>
      <section class='content'>
        <div class='contact'>
          <div>
            <form>
              <label for='name'>Your Name</label>
              <input type='text' name='name' id='name'/>
              <label for='name'>Your Email</label>
              <input type='text' name='name' id='name'/>
              <label for='name'>Your Message</label>
              <textarea name='message' id='message'></textarea>
              <button type='submit'>Submit</button>
            </form>
          </div>
            
          <div>
            <div>
              <h3>Let's chat about your project</h3>
              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
              <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aperiam quos nulla ullam doloremque voluptatum veritatis iste ea accusantium eum eos delectus tempore, earum deserunt nihil! Fugiat deserunt natus suscipit quas.</p>
              <p>Office Hours: <br> Mon - Fri &ensp;  8:30AM - 6:30PM PST</p>
            </div>
          </div>
        </div>
      </section>
    </section>
