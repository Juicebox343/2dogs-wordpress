<?php

function twodogs_files() {
  wp_enqueue_style('custom-fonts', '//fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap');
  wp_enqueue_style('twodogs_style', get_theme_file_uri('/style.css'));
}

add_action('wp_enqueue_scripts', 'twodogs_files');

?>