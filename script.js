const serviceRadioBtns = document.querySelectorAll('input[name="service"]')
const serviceDescriptions = document.querySelectorAll('.service-description > div')


serviceRadioBtns.forEach((button, index) => {
  button.addEventListener('change', (event) =>{
    serviceDescriptions.forEach((description) =>{
      description.classList.remove('active-service')
    })
    serviceDescriptions[index].classList.add('active-service')
})})

